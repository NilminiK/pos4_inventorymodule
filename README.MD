> OMAK POS 4 - Backend API

#### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm run test OR node app.js
```
#### Testing API with POSTMAN

For SignUP

1. Create a POST method with header value 'http://localhost:3000/users/signup'
2. Body contents as JSON: 
{
	"email":"niki@gmail.com",
	"password":"123"
}

For SignIN 

1. Create a POST method with header value 'http://localhost:3000/users/signIn'
2. Body contents as JSON: 
{
	"email":"niki@gmail.com",
	"password":"123"
}
---

Please reffer Inv Model definitions on: 

* [Purchase Order](https://bitbucket.org/NilminiK/pos4_inventorymodule/wiki/PurchaseOrder)

* [GRN](https://bitbucket.org/NilminiK/pos4_inventorymodule/wiki/Goods%20Received%20Note)

* [InventoryDetail](https://bitbucket.org/NilminiK/pos4_inventorymodule/wiki/InventoryDetail)

* [InventoryDetailHistory](https://bitbucket.org/NilminiK/pos4_inventorymodule/wiki/InventoryDetailHistory)

* [Stock Varience](https://bitbucket.org/NilminiK/pos4_inventorymodule/wiki/Stock%20Varience)