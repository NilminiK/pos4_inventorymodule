const uuid = require('node-uuid-generator');
const bcrypt = require('bcryptjs');
const db = require("../base");

class User {

    // getByID(id) {
    //     let where = {
    //         "_id": id
    //     }

    //     return new Promise(() => {
    //         db.dbHandle.find(where)
    //             .then(user => {
    //                 return user;
    //             })
    //             .catch(err => {
    //                 return err;
    //             });
    //     });
    // };

    async findByAttribute(email) {
        try {
            let where = {
                "selector": {
                    "email": {
                        "$eq": email
                    }
                }
            };
            var user = await db.dbHandle.find(where);
            if (user.docs[0]) {
                return user.docs[0];
            }
            else {
                return new Error("no data found")

            }
        } catch (error) {
            return error;
        }

    };

    async createUser(user) {
        try {
            user._id = 'User_' + uuid.generate();
            var salt = await bcrypt.genSalt(10);
            user.password = await bcrypt.hash(user.password, salt);
            await db.dbHandle.insert(user);
            return user;
        } catch (error) {
            return error;
        }
    };

    async isValidPassword(userPassword, password) {
       return await bcrypt.compare(password, userPassword);
    }
}

module.exports = User;